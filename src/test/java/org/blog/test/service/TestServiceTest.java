package org.blog.test.service;

import lombok.extern.slf4j.Slf4j;
import org.blog.test.AsyncTestApplication;
import org.blog.test.config.AsyncConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.junit4.*;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TestServiceTest {

    @Configuration
    @Import(AsyncTestApplication.class)
    static class ContextConfiguration {
        @Bean
        @Primary
        public Executor executor() {
            return new SyncTaskExecutor();
        }
    }

    @Autowired
    private TestService testService;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void testGetString() {

        //given
        given(restTemplate.getForEntity(anyString(), eq(String.class))).willReturn(new ResponseEntity<String>(HttpStatus.OK));

        //when
        testService.getString();

        //then
        verify(restTemplate, times(1)).getForEntity(anyString(), eq(String.class));
    }
}
