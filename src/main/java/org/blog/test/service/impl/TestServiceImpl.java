package org.blog.test.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.blog.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    @Async
    public void getString() {

        log.info("start");
        restTemplate.getForEntity("http://www.test.com", String.class);
    }
}
