package org.blog.test.service;

import org.springframework.scheduling.annotation.Async;

public interface TestService {
    @Async
    void getString();
}
